import common
import helpers


def _solve() -> int:
    priorities = common.get_priorities()
    tot = 0

    for backpack_data in helpers.get_data():
        middle = len(backpack_data) // 2

        first_half, second_half = set(backpack_data[:middle]), set(backpack_data[middle:])

        for i in first_half & second_half:
            tot += priorities[i]

    return tot


if __name__ == '__main__':
    print(_solve())
    print(helpers.mesure_exec_time(_solve))
