import time
from typing import Callable, Iterator


def get_data() -> Iterator[str]:
    with open("input.txt") as f:
        for line in f.readlines():
            yield line.rstrip("\n")


def mesure_exec_time(solution: Callable) -> float:
    st = time.time()
    solution()
    ed = time.time()

    return ed - st
