import common
import helpers


def _solve() -> int:
    count = 0

    for line in helpers.get_data():
        first_range, second_range = common.get_ranges(line)

        if first_range[1] >= second_range[0] and first_range[0] <= second_range[1]:
            count += 1

    return count


if __name__ == '__main__':
    print(_solve())
    print(helpers.mesure_exec_time(_solve))
