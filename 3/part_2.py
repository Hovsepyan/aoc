import common
import helpers


def _solve() -> int:
    priorities = common.get_priorities()
    tot = 0

    full_group = []

    for backpack_data in helpers.get_data():
        full_group.append(backpack_data)

        if len(full_group) == 3:
            for i in set(full_group[0]) & set(full_group[1]) & set(full_group[2]):
                tot += priorities[i]

            full_group.clear()

    return tot


if __name__ == '__main__':
    print(_solve())
    print(helpers.mesure_exec_time(_solve))
