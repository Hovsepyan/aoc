def get_priorities():
    letters = [chr(97 + i) for i in range(26)]
    priorities = {}

    for i, l in enumerate(letters, 1):
        priorities[l] = i
        priorities[l.upper()] = i + 26

    return priorities
