def get_ranges(line: str) -> tuple[list[int], list[int]]:
    first_range, second_range = line.split(",")

    first_range = [int(i) for i in first_range.split("-")]
    second_range = [int(i) for i in second_range.split("-")]

    return first_range, second_range
