import common
import helpers


def _solve() -> int:
    count = 0

    for line in helpers.get_data():
        first_range, second_range = common.get_ranges(line)

        for f_r, s_r in ((first_range, second_range), (second_range, first_range)):
            if f_r[0] <= s_r[0] and f_r[1] >= s_r[1]:
                count += 1

                break

    return count


if __name__ == '__main__':
    print(_solve())
    print(helpers.mesure_exec_time(_solve))
